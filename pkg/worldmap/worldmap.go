// Copyright (c) 2023 Salvatore Mazzarino
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//	http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package worldmap

import (
	"errors"
	"fmt"
	"math/rand"
	"time"

	"github.com/dominikbraun/graph"

	"gitlab.com/mazzy/alieninvasion/pkg/city"
)

type WorldMap struct {
	graph.Graph[string, city.City]

	Cities  []city.City
	Streets []graph.Edge[city.City]
}

// New creates a new world map reading the content from the filename.
func New(filename string) (WorldMap, error) {
	g := graph.New(city.Hash)

	wm := WorldMap{Graph: g}

	file, err := readFile(filename)
	if err != nil {
		return WorldMap{}, err
	}

	mp := tokenizeFile(file)

	// we first add all the cities
	for cityName := range mp {
		wm.AddCity(cityName)
	}

	// we create now all the connection between the cities (the edges of our graph)
	for cityName, directions := range mp {
		for i := 0; i < len(directions)-1; i++ {
			cardinalPoint := directions[i]
			cityNameDst := directions[i+1]

			wm.AddStreet(cityName, cityNameDst, cardinalPoint)
		}
	}

	return wm, nil
}

// RandomCity picks a random city from all the available ones in the world map.
func (wm *WorldMap) RandomCity() city.City {
	s := rand.NewSource(time.Now().UnixNano())
	r := rand.New(s)

	nCities := len(wm.Cities)

	return wm.Cities[r.Intn(nCities)]
}

// DestroyCities destroys all the cities and all the streets connected to it.
func (wm *WorldMap) DestroyCities(cities []city.City) error {
	// we filter with all the destroyed cities
	for _, city := range cities {
		// AdjacencyMap computes an adjacency map with all cities in the graph.
		adjacencyMap, _ := wm.AdjacencyMap()

		directions := adjacencyMap[city.Name]

		for _, d := range directions {
			if err := wm.RemoveEdge(d.Target, d.Source); err != nil {
				return err
			}
		}

		if err := wm.RemoveVertex(city.Name); err != nil {
			return err
		}
	}

	return nil
}

func (wm *WorldMap) AddCity(name string) error {
	city := city.New(name)

	if err := wm.AddVertex(city); err != nil {
		return err
	}

	wm.Cities = append(wm.Cities, city)

	return nil
}

func (wm *WorldMap) AddStreet(start, end, cardinalPoint string) error {
	if err := wm.AddEdge(start, end, graph.EdgeAttribute("cardinalPoint", cardinalPoint)); err != nil {
		// in case the edge already exist we do not throw an error
		if !errors.Is(err, graph.ErrEdgeAlreadyExists) {
			return err
		}
	}

	wm.Streets = append(wm.Streets, graph.Edge[city.City]{
		Source: city.New(start),
		Target: city.New(end),
		Properties: graph.EdgeProperties{
			Attributes: map[string]string{
				"cardinalPoint": cardinalPoint,
			},
		},
	})

	return nil
}

func (wm *WorldMap) Report() error {
	adjacencyMap, _ := wm.AdjacencyMap()

	for city, streets := range adjacencyMap {
		var directions []string
		for _, street := range streets {
			d := fmt.Sprintf("%s=%s", street.Properties.Attributes["cardinalPoint"], street.Target)
			directions = append(directions, d)
		}

		line := city
		for i, d := range directions {
			if i == 0 {
				line += fmt.Sprintf(" %s ", d)
				continue
			}

			if i == len(directions)-1 {
				line += d
				continue
			}

			line += fmt.Sprintf("%s ", d)
		}

		if err := writeLine([]byte(line)); err != nil {
			return err
		}
	}

	return nil
}
