// Copyright (c) 2023 Salvatore Mazzarino
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//	http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package invasion

import (
	"testing"

	"github.com/dominikbraun/graph"
	"github.com/stretchr/testify/assert"

	"gitlab.com/mazzy/alieninvasion/pkg/city"
	"gitlab.com/mazzy/alieninvasion/pkg/worldmap"
)

func TestInvasion_Start(t *testing.T) {
	wm := worldmap.WorldMap{
		Graph: graph.New(city.Hash),
	}

	wm.AddCity("Catania")
	wm.AddCity("Siracusa")
	wm.AddCity("Messina")
	wm.AddCity("Palermo")

	wm.AddStreet("Catania", "Siracusa", "south")
	wm.AddStreet("Catania", "Messina", "north")
	wm.AddStreet("Siracusa", "Catania", "north")
	wm.AddStreet("Messina", "Palermo", "west")
	wm.AddStreet("Palermo", "Siracusa", "south")

	invasion, err := New(3, wm)
	assert.NoError(t, err)

	invasion.Aliens[0].CurrentCity.Name = "Messina"
	invasion.Aliens[1].CurrentCity.Name = "Palermo"
	invasion.Aliens[2].CurrentCity.Name = "Siracusa"

	err = invasion.Start()
	assert.NoError(t, err)

	for _, a := range invasion.Aliens {
		assert.NotEmpty(t, a.CurrentCity)
	}
}

func TestInvasion_Start_AllAliensInTwoDifferentSameCities(t *testing.T) {
	wm := worldmap.WorldMap{
		Graph: graph.New(city.Hash),
	}

	wm.AddCity("Catania")
	wm.AddCity("Siracusa")
	wm.AddCity("Messina")
	wm.AddCity("Palermo")

	wm.AddStreet("Catania", "Siracusa", "south")
	wm.AddStreet("Catania", "Messina", "north")
	wm.AddStreet("Siracusa", "Catania", "north")
	wm.AddStreet("Messina", "Palermo", "west")
	wm.AddStreet("Palermo", "Siracusa", "south")

	invasion, err := New(4, wm)
	assert.NoError(t, err)

	invasion.Aliens[0].CurrentCity.Name = "Catania"
	invasion.Aliens[1].CurrentCity.Name = "Messina"
	invasion.Aliens[2].CurrentCity.Name = "Messina"
	invasion.Aliens[3].CurrentCity.Name = "Catania"

	err = invasion.Start()
	assert.NoError(t, err)
}
