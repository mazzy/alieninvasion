module gitlab.com/mazzy/alieninvasion

go 1.20

require (
	github.com/dominikbraun/graph v0.22.3
	github.com/stretchr/testify v1.8.4
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
