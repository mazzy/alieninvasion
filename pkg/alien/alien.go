// Copyright (c) 2023 Salvatore Mazzarino
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//	http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package alien

import (
	"fmt"
	"math/rand"
	"strings"
	"time"

	"gitlab.com/mazzy/alieninvasion/pkg/city"
	"gitlab.com/mazzy/alieninvasion/pkg/worldmap"
)

// Alien represents an alien that is going to attack the world.
type (
	Alien struct {
		ID          string
		CurrentCity city.City
		Travels     int
		IsAlive     bool
	}

	Aliens []*Alien
)

// New creates a new alien and place him in the starting city.
func New(id string) Alien {
	return Alien{
		ID:      id,
		Travels: 0,
		IsAlive: true,
	}
}

func (a *Alien) SetStartingCity(city city.City) {
	a.CurrentCity = city
}

// IsExhausted checks whether the alien travelled more than 10000 times.
func (a *Alien) IsExhausted() bool {
	return a.Travels > 10000
}

// Move moves randomly an alien in all the possible allowed direction in the world map.
func (a *Alien) Move(wm worldmap.WorldMap) {
	// AdjacencyMap computes an adjacency map with all cities in the graph.
	adjacencyMap, _ := wm.AdjacencyMap()

	directions := adjacencyMap[a.CurrentCity.Name]
	cities := make([]string, len(directions))

	if len(directions) == 0 {
		fmt.Printf("the alien %s is trapped\n", a.ID)
		return
	}

	i := 0
	for d := range directions {
		cities[i] = d
		i++
	}

	s := rand.NewSource(time.Now().UnixNano())
	r := rand.New(s)

	nextRandCityName := cities[r.Intn(len(cities))]
	a.CurrentCity = city.New(nextRandCityName)
	a.Travels++
}

// AllAliveExhausted checks whether all aliens have travelled more than 10000 times.
func (as Aliens) AllAliveExhausted() bool {
	for _, a := range as {
		if !a.IsAlive {
			continue
		}

		if !a.IsExhausted() {
			return false
		}
	}

	return true
}

// AreAlive checks whether all the aliens are still alive.
func (as Aliens) AreAlive() bool {
	for _, a := range as {
		if a.IsAlive {
			return true
		}
	}

	return false
}

// Fight builds an in-memory map to check whether in the same city there are
// located multiple aliens and kills all of them that are in the same city and
// at the end of the fight return the list of cities destroyed.
func (as Aliens) Fight() []city.City {
	var destroyedCities []city.City

	locations := make(map[city.City]Aliens, len(as))

	for _, a := range as {
		if !a.IsAlive {
			continue
		}

		currCity := a.CurrentCity
		locations[currCity] = append(locations[currCity], a)
	}

	for city, aliens := range locations {
		// it means only one alien landed in that city
		// and no city will be destroyed
		if len(aliens) == 1 {
			continue
		}

		// kills all of them in the fight
		for _, a := range aliens {
			a.IsAlive = false

			// we mark here the city as destroyed but only after
			// we are going to remove it from the world map and
			// erase all its streets
			a.CurrentCity.Destroy()
		}

		fmt.Printf("%s has been destroyed by %s\n", city.Name, formatMessage(aliens))

		city.Destroy()
		destroyedCities = append(destroyedCities, city)
	}

	return destroyedCities
}

func formatMessage(aliens Aliens) string {
	var msg strings.Builder

	for i, a := range aliens {
		if i == 0 {
			msg.Write([]byte(fmt.Sprintf("alien %s", a.ID)))
			continue
		}

		if i == len(aliens)-1 {
			msg.Write([]byte(fmt.Sprintf(" and alien %s.", a.ID)))
			continue
		}

		msg.Write([]byte(fmt.Sprintf(", alien %s", a.ID)))
	}

	return msg.String()
}
