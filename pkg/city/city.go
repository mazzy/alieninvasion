// Copyright (c) 2023 Salvatore Mazzarino
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//	http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package city

type (
	City struct {
		Name        string
		IsDestroyed bool
	}
)

// New creates a new city with a name.
func New(name string) City {
	return City{
		Name:        name,
		IsDestroyed: false,
	}
}

// Destroy sets the city as destroyed after aliens attacked.
func (c *City) Destroy() {
	c.IsDestroyed = true
}

func Hash(c City) string {
	return c.Name
}
