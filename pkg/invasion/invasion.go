// Copyright (c) 2023 Salvatore Mazzarino
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//	http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package invasion

import (
	"errors"
	"strconv"

	"gitlab.com/mazzy/alieninvasion/pkg/alien"
	"gitlab.com/mazzy/alieninvasion/pkg/worldmap"
)

var ErrAliensCountNotValid = errors.New("aliens number is not valid. Valid values are greater than 0")

type (
	Invasion struct {
		Aliens   alien.Aliens
		WorldMap worldmap.WorldMap
	}
)

// New creates a new alien invasion.
func New(n uint, wm worldmap.WorldMap) (Invasion, error) {
	invasion := Invasion{
		WorldMap: wm,
	}

	if n == 0 {
		return Invasion{}, ErrAliensCountNotValid
	}

	for i := 0; uint(i) < n; i++ {
		id := strconv.Itoa(i)

		alien := alien.New(id)
		invasion.Aliens = append(invasion.Aliens, &alien)
	}

	return invasion, nil
}

// Start run the invasion of the alien on the world map and return
// when either all the aliens are dead or they have travelled
// more than 10000 times.
func (i Invasion) Start() error {
	// there might be the case when the aliens land
	// in the same city initially. For that reason we
	// need to simulate the fight immediately
	destroyedCities := i.Aliens.Fight()
	if len(destroyedCities) > 0 {
		if err := i.WorldMap.DestroyCities(destroyedCities); err != nil {
			return err
		}
	}

	// run until all the aliens are destroyed or exhausted
	// hop in the graph for more than 10000 times
	for i.Aliens.AreAlive() && !i.Aliens.AllAliveExhausted() {
		for _, a := range i.Aliens {
			if !a.IsAlive {
				continue
			}

			a.Move(i.WorldMap)
		}

		// simulate an alien fight
		// if they are in the same city, they destroy the city and themselves
		destroyedCities := i.Aliens.Fight()

		if len(destroyedCities) > 0 {
			if err := i.WorldMap.DestroyCities(destroyedCities); err != nil {
				return err
			}
		}
	}

	return nil
}

// Debrief prints out in a file the results of the alien invasion
// printing in the same format as the original file what it is left.
func (i Invasion) Debrief() error {
	return i.WorldMap.Report()
}
