# Alien Invasion

Alien Invasion Mad is about a simulation of mad aliens that invade the earth.

## Walkthrough

The application is build in the form of a command-line. It accepts as arguments:

- `n`: the number of aliens that are going to attack the world
- `file`: the path where the map txt file is located

```shell
Usage of build/alieninvasion:
  -file string
        Path of the file containing the world map
  -n uint
        Numbers of aliens to create (default 5)

```

Create a file where each list is a city and all the connections to other cities.

The format must be as the follow:

```txt
Catania south=Siracusa north=Messina
Siracusa north=Catania
Messina west=Palermo
Palermo south=Siracusa
```

## Build

```shell
$ make build
```

## Run

```shell
$ ./build/alieninvasion --n 3 --file map.txt
```

The simulation creates a file `reports.txt` under the same directory where the simulation runs. The file shows with the same
structure of the `map.txt` file what is left after the simulation.

## Tests

Unit tests are provided to test the core functionalities of the simulation.

Run:

```sh
$ make test
```

## Assumptions

- If two or more aliens land in the same city, all the aliens are destroyed and the city as well.
- An alien moves randomly towards all the possible existent directions.
