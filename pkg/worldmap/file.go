// Copyright (c) 2023 Salvatore Mazzarino
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//	http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package worldmap

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

func readFile(filename string) ([]string, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, fmt.Errorf("an error occurred during the opening of the file %s: %w", filename, err)
	}

	defer file.Close()

	content, err := readLines(file)
	if err != nil {
		return nil, fmt.Errorf("an error occurred during the reading of the file %s: %w", filename, err)
	}

	return content, nil
}

// readLines reads scan the input source and return the content in a slice.
func readLines(reader io.Reader) ([]string, error) {
	var lines []string

	scanner := bufio.NewScanner(reader)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		return nil, fmt.Errorf("an error occurred during the reading: %w", err)
	}

	return lines, nil
}

// tokenizeFile reads the content of the world map file and create a map
// which as key contain the city and as values a slice with the cardinal points
// and the city connected to it.
func tokenizeFile(file []string) map[string][]string {
	mp := make(map[string][]string)

	for _, line := range file {
		token := strings.Split(line, " ")

		city := token[0]
		for _, d := range token[1:] {
			mp[city] = append(mp[city], tokenizeDirection(d)...)
		}
	}

	return mp
}

func tokenizeDirection(dst string) []string {
	return strings.Split(dst, "=")
}

func writeLine(line []byte) error {
	f, err := os.OpenFile("reports.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0o644)
	if err != nil {
		return err
	}

	defer f.Close()

	fl := fmt.Sprintf("%s\n", line)
	if _, err := f.WriteString(fl); err != nil {
		return err
	}

	return nil
}
