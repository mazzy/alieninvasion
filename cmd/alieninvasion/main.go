// Copyright (c) 2023 Salvatore Mazzarino
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//	http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"flag"
	"fmt"
	"log"

	"gitlab.com/mazzy/alieninvasion/pkg/invasion"
	"gitlab.com/mazzy/alieninvasion/pkg/worldmap"
)

func main() {
	var aliensCount uint
	var filename string

	flag.UintVar(&aliensCount, "n", 5, "Numbers of aliens to create")
	flag.StringVar(&filename, "file", "", "Path of the file containing the world map")
	flag.Parse()

	wm, err := worldmap.New(filename)
	if err != nil {
		log.Fatal(fmt.Errorf("an error occurred during the creation of the world map: %w", err))
	}

	i, err := invasion.New(aliensCount, wm)
	if err != nil {
		log.Fatal(fmt.Errorf("an error occurred during the creation of a new invasion: %w", err))
	}

	for _, alien := range i.Aliens {
		// pick a random city where to place the alien
		randCity := i.WorldMap.RandomCity()
		alien.SetStartingCity(randCity)
	}

	if err := i.Start(); err != nil {
		log.Fatal(fmt.Errorf("error: %w", err))
	}

	if err := i.Debrief(); err != nil {
		log.Fatal(fmt.Errorf("error: %w", err))
	}
}
