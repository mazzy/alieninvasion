// Copyright (c) 2023 Salvatore Mazzarino
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//	http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package worldmap

import (
	"os"
	"testing"

	"github.com/dominikbraun/graph"
	"github.com/stretchr/testify/assert"

	"gitlab.com/mazzy/alieninvasion/pkg/city"
)

func TestWorldMap_CreateNewWorldMap(t *testing.T) {
	expectedWorldMap := WorldMap{
		Graph: graph.New(city.Hash),
		Cities: []city.City{
			{
				Name:        "Catania",
				IsDestroyed: false,
			},
			{
				Name:        "Siracusa",
				IsDestroyed: false,
			},
			{
				Name:        "Messina",
				IsDestroyed: false,
			},
			{
				Name:        "Palermo",
				IsDestroyed: false,
			},
		},
		Streets: []graph.Edge[city.City]{
			{
				Source: city.City{Name: "Catania"},
				Target: city.City{Name: "Siracusa"},
				Properties: graph.EdgeProperties{
					Attributes: map[string]string{
						"cardinalPoint": "south",
					},
				},
			},
			{
				Source: city.City{Name: "Catania"},
				Target: city.City{Name: "Messina"},
				Properties: graph.EdgeProperties{
					Attributes: map[string]string{
						"cardinalPoint": "north",
					},
				},
			},
			{
				Source: city.City{Name: "Siracusa"},
				Target: city.City{Name: "Catania"},
				Properties: graph.EdgeProperties{
					Attributes: map[string]string{
						"cardinalPoint": "north",
					},
				},
			},
			{
				Source: city.City{Name: "Messina"},
				Target: city.City{Name: "Palermo"},
				Properties: graph.EdgeProperties{
					Attributes: map[string]string{
						"cardinalPoint": "west",
					},
				},
			},
			{
				Source: city.City{Name: "Palermo"},
				Target: city.City{Name: "Siracusa"},
				Properties: graph.EdgeProperties{
					Attributes: map[string]string{
						"cardinalPoint": "south",
					},
				},
			},
		},
	}

	const file = `Catania south=Siracusa north=Messina
Siracusa north=Catania
Messina west=Palermo
Palermo south=Siracusa
`

	w, err := os.CreateTemp("", "")
	defer os.Remove(w.Name())

	assert.NoError(t, err)

	_, err = w.Write([]byte(file))
	assert.NoError(t, err)

	wm, err := New(w.Name())
	assert.NoError(t, err)

	assert.ElementsMatch(t, expectedWorldMap.Cities, wm.Cities)
	assert.ElementsMatch(t, expectedWorldMap.Streets, wm.Streets)
}

func TestWorldMap_DestroyCities(t *testing.T) {
	wm := WorldMap{
		Graph: graph.New(city.Hash),
	}

	wm.AddCity("Catania")
	wm.AddCity("Siracusa")
	wm.AddCity("Messina")
	wm.AddCity("Palermo")

	wm.AddStreet("Catania", "Siracusa", "south")
	wm.AddStreet("Catania", "Messina", "north")
	wm.AddStreet("Siracusa", "Catania", "north")
	wm.AddStreet("Messina", "Palermo", "west")
	wm.AddStreet("Palermo", "Siracusa", "south")

	_ = wm.DestroyCities([]city.City{
		{
			Name:        "Catania",
			IsDestroyed: true,
		},
	})

	_, err := wm.Vertex("Catania")
	assert.ErrorAs(t, err, &graph.ErrVertexNotFound)

	_, err = wm.Edge("Siracusa", "Catania")
	assert.ErrorAs(t, err, &graph.ErrEdgeNotFound)
	_, err = wm.Edge("Catania", "Siracusa")
	assert.ErrorAs(t, err, &graph.ErrEdgeNotFound)
	_, err = wm.Edge("Catania", "Messina")
	assert.ErrorAs(t, err, &graph.ErrEdgeNotFound)
}
