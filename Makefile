.PHONY: build test coverage clean

build:
	CGO_ENABLED=0 go build -ldflags "-s -w" -o build/alieninvasion cmd/main.go

test:
	go test ./...

coverage:
	go test ./... -coverprofile=coverage.out
	go tool cover -html=coverage.out


clean:
	go clean
	rm -rf build/
	rm -rf reports.txt
