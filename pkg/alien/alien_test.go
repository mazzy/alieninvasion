// Copyright (c) 2023 Salvatore Mazzarino
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//	http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package alien

import (
	"strconv"
	"testing"

	"github.com/dominikbraun/graph"
	"github.com/stretchr/testify/assert"

	"gitlab.com/mazzy/alieninvasion/pkg/city"
	"gitlab.com/mazzy/alieninvasion/pkg/worldmap"
)

func TestAlien_MoveAlienOnce(t *testing.T) {
	wm := worldmap.WorldMap{
		Graph: graph.New(city.Hash),
	}

	wm.AddCity("Catania")
	wm.AddCity("Siracusa")
	wm.AddCity("Messina")
	wm.AddCity("Palermo")

	wm.AddStreet("Catania", "Siracusa", "south")
	wm.AddStreet("Catania", "Messina", "north")
	wm.AddStreet("Siracusa", "Catania", "north")
	wm.AddStreet("Messina", "Palermo", "west")
	wm.AddStreet("Palermo", "Siracusa", "south")

	alien := New("2")
	alien.SetStartingCity(city.New("Catania"))

	alien.Move(wm)

	assert.NotEqual(t, "Catania", alien.CurrentCity.Name)
	assert.Equal(t, 1, alien.Travels)
}

func TestAliens_Fight(t *testing.T) {
	expectedDestroyedCities := []city.City{
		{
			Name:        "Catania",
			IsDestroyed: true,
		},
	}

	aliens := Aliens{
		{
			ID:          "34",
			CurrentCity: city.City{Name: "Catania"},
			Travels:     1,
			IsAlive:     true,
		},
		{
			ID:          "25",
			CurrentCity: city.City{Name: "Catania"},
			Travels:     1,
			IsAlive:     true,
		},
	}

	destroyedCities := aliens.Fight()

	assert.ElementsMatch(t, expectedDestroyedCities, destroyedCities)
	assert.Equal(t, false, aliens.AreAlive())
}

func TestAliens_AreAlive_AllAliensKilled(t *testing.T) {
	var aliens Aliens

	for i := 0; i < 10; i++ {
		a := New(strconv.Itoa(i))
		a.IsAlive = false
		aliens = append(aliens, &a)
	}

	assert.Equal(t, false, aliens.AreAlive())
}

func TestAliens_AreAlive_AllAliensKilled_ExceptOne(t *testing.T) {
	var aliens Aliens

	for i := 0; i < 10; i++ {
		a := New(strconv.Itoa(i))

		if i == 5 {
			a.IsAlive = true
		} else {
			a.IsAlive = false
		}

		aliens = append(aliens, &a)
	}

	assert.Equal(t, true, aliens.AreAlive())
}

func TestAliens_AllExhausted(t *testing.T) {
	var aliens Aliens

	for i := 0; i < 10; i++ {
		a := New(strconv.Itoa(i))

		if i == 4 {
			a.IsAlive = false
		} else {
			a.IsAlive = true
		}

		a.Travels = 10001
		aliens = append(aliens, &a)
	}

	assert.Equal(t, true, aliens.AllAliveExhausted())
}
